# Build stage
FROM alpine AS build-stage

RUN apk add --no-cache \
    wget \
    build-base \
    zlib-dev \
    pcre-dev \
    libressl-dev \
    openssl \
    linux-headers

ARG NGINX_VERSION=1.23.4
RUN wget https://nginx.org/download/nginx-$NGINX_VERSION.tar.gz && \
    tar -zxvf nginx-$NGINX_VERSION.tar.gz && \
    rm nginx-$NGINX_VERSION.tar.gz && \
    cd nginx-$NGINX_VERSION

RUN cd nginx-$NGINX_VERSION && \
    ./configure \
        --with-ld-opt="-static" \
        --without-http_gzip_module \
        --with-http_ssl_module \
        --prefix=/nginx && \
    make install && \
    strip /nginx/sbin/nginx

# Run stage
FROM scratch

COPY --from=build-stage /nginx/sbin/nginx /nginx_binary
COPY --from=build-stage /etc/passwd /etc/group /etc/
COPY --from=build-stage /nginx /nginx

COPY nginx.conf /nginx.conf

STOPSIGNAL SIGQUIT

EXPOSE 80

ENTRYPOINT ["/nginx_binary"]
CMD ["-c", "/nginx.conf", "-g", "daemon off;"]
